#coding: utf-8
'''
用來導入 Brython 網際運算環境
'''
def BrythonConsole():
    return '''
<script src="/Brython1.2-20131109-201900/brython.js"></script>
<script>
window.onload = function(){
    brython(1);
}
</script><script type="text/python">
import sys
import time
import dis

if sys.has_local_storage:
    from local_storage import storage
else:
    storage = False

def write(data):
    doc["console2"].value += str(data)

#sys.stdout = object()    #not needed when importing sys via src/Lib/sys.py
sys.stdout.write = write


def clear_text():
    log("event clear")
    doc['console2'].value=''
    #doc['src'].value=''

def run():
    global output
    doc["console2"].value=''
    doc["console2"].cols = 60
    src = doc["src"].value
    if storage:
        storage["py_src"]=src
    t0 = time.time()
    exec(src)
    output = doc["console2"].value
    print('<done in %s ms>' %(time.time()-t0))

def show_js():
    src = doc["src"].value
    doc["console2"].cols = 90
    doc["console2"].value = dis.dis(src)

</script>
<table width=80%>
<tr><td style="text-align:center"><b>Python</b>
</td>
<td>&nbsp;</td>
<th><input type="button" value="Javascript" onClick="show_js()"></th>
</tr>
<tr><td><div id="editor"></div><textarea id="src" name=form_34ed066df378efacc9b924ec161e7639program cols="60" rows="20">
o = 1
k = 1
while o <= 9:
    while k <= 9:
        print(o ," x " ,k ," = " ,o * k )
        k += 1
    o += 1
    k = 1
    print()</textarea></td><td><input type="button" value="Run" onClick="run()"></td>
<td><input type="button" value="Clear Output" onClick="clear_text()">
</td>
<td colspan=2><textarea id="console2" cols="60" rows="20"></textarea></td>
</tr>
<tr><td colspan=2>


'''