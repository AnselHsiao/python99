#coding: utf-8

# 第一層標題
m1 = "<a href='/about'>有關本站</a>"
m11 = "<a href='/'>Brython程式區</a>"
# 第一層標題
#m3 = "<a href=''>Solvespace 零件區</a>"
#m31 = "<a href='/Solvespaceparts'>Solvespace 零件繪圖</a>"
#m32 = "<a href='/Solvespace3Ds'>Solvespace 組立操作</a>"
# 第一層標題
#m4 = "<a href=''>Creo 零件區</a>"
#m41 = "<a href='/creoparts'>Creo 零件繪圖</a>"
#m42 = "<a href='/creo3Ds'> Creo 組立操作</a>"
#m43 = "<a href='/Pro_Web'>Pro/Web.Link</a>"
# 第一層標題
#m5 = "<a href=''>V-REP</a>"
#m51 = "<a href='/VREPDO'>V-REP 操作</a>"
# 第一層標題
#m2 = "<a href=''>個人介紹</a>"
#m21 = "<a href='/introMember'>11號-蕭莉蓉</a>"

# 請注意, 數列第一元素為主表單, 隨後則為其子表單
表單數列 = [ \
        [m1, m11],\
        ]
        #[m2, m21], \
        #[m3, m31, m32], \
        #[m4, m41,m42,m43],\
        #[m5,m51],\
        #]

def SequenceToUnorderedList(seq, level=0):
    # 只讓最外圍 ul 標註 class = 'nav'
    if level == 0:
        html_code = "<ul class='nav'>"
    # 子表單不加上 class
    else:
        html_code = "<ul>"
    for item in seq:
        if type(item) == type([]):
            level += 1
            html_code +="<li>"+ item[0]
            # 子表單 level 不為 0
            html_code += SequenceToUnorderedList(item[1:], 1)
            html_code += "</li>"
        else:
            html_code += "<li>%s</li>" % item
    html_code += "</ul>\n"
    return html_code

def GenerateMenu():
    return SequenceToUnorderedList(表單數列, 0) \
+'''
<script type="text/javascript" src="/static/jscript/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="/static/jscript/dropdown.js"></script>
'''
